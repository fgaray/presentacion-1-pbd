# Contexto

## Eventos

Una gran cantidad de eventos en todo el mundo se han visto beneficiados debido a
las redes sociales que logran un nivel de difusión que permite crear eventos
masivos en solo unos momentos.


## Crowdfunding 

El crowdfunding o financiación masiva ha permitido que personas puedan llevar a
cabo proyectos que de otra forma no podrían realizarse.

\begin{figure}[t]

	\centering
	\includegraphics[scale=0.3]{crowdfunding_money.jpg}
	\caption{El crowdfunding}

\end{figure}

# Problema


## Problema

Falta de financiamiento para eventos pequeños lo que les dificulta conseguir
auspiciadores.



# Solución
## Solución

Una plataforma web que permita publicar eventos para que estos sean financiados
de forma anticipada por interesados y que además ofresca premios en forma de
productos o servicios dados por auspiciadores cuando un evento cumpla con los
requisitos de un auspiciador.




# Proyectos similares

## Eventioz

eventioz.com es una página que permite crear, gestionar y compartir eventos así
como vender tikets para eventos de pago.

\begin{figure}[t]

	\centering
	\includegraphics[scale=0.5]{eventioz.png}
	\caption{Logo de Eventioz}

\end{figure}


## KickStarter


KickStarter permite a personas publicar proyectos que requieren de dinero para
ser realizados. A cambio, los que aportan con dinero tienen ofertas especiales
como por ejemplo acceso anticipado al producto.


\begin{figure}[t]

	\centering
	\includegraphics[scale=0.3]{kickstarter.png}
	\caption{Logo de KickStarter}

\end{figure}


## Diferencias con la solución


OurEvent ofrecerá la capacidad de reunir dinero antes que el evento sea fijado,
ademas de ofrecer premios o recompensas que auspiciadores podrán ofrecer a
eventos de su interés.


# Diagramas de flujo de datos

## Diagrama de Contexto


\begin{figure}[t]

	\centering
	\includegraphics[width=\textwidth,height=\textheight,keepaspectratio]{imagenes/contexto.png}
\end{figure}


## Diagrama de nivel 0

\begin{figure}[t]

	\centering
	\includegraphics[width=\textwidth,height=\textheight,keepaspectratio]{imagenes/DFD0.png}
\end{figure}


## Diagrama de nivel 1 (Auspiciadores)


\begin{figure}[t]

	\centering
	\includegraphics[width=\textwidth,height=\textheight,keepaspectratio]{imagenes/DFD1_auspiciador.png}
\end{figure}


## Diagrama de nivel 1 (Organizadores)



\begin{figure}[t]

	\centering
	\includegraphics[width=\textwidth,height=\textheight,keepaspectratio]{imagenes/DFD1_organizador.png}
\end{figure}



## Diagrama de nivel 1 (Visitantes)



\begin{figure}[t]

	\centering
	\includegraphics[width=\textwidth,height=\textheight,keepaspectratio]{imagenes/DFD1_visitantes.png}
\end{figure}



## Diagrama de nivel 1 (Servicios)

\begin{figure}[t]

	\centering
	\includegraphics[width=\textwidth,height=\textheight,keepaspectratio]{imagenes/DFD_servicio.png}
\end{figure}


# Business model canvas

## Canvas


\begin{figure}[t]

	\centering
	\includegraphics[width=\textwidth,height=\textheight,keepaspectratio]{imagenes/canvas.png}
\end{figure}



# Modelo de datos

## Modelo de datos


\begin{figure}[t]

	\centering
	\includegraphics[width=\textwidth,height=\textheight,keepaspectratio]{imagenes/modelo_de_datos.png}
\end{figure}

